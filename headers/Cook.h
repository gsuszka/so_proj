#pragma once

#include <ncurses.h>
#include <random>
#include <vector>

struct Cook {
    static int IDs;
    static Cook None;
    enum STATE {
        IDLE, COOKING
    };

    int ID = -1;
    int cookingTime = 0;
    float progress = 0.0;
    STATE state = IDLE;
    std::pair<int, int> coord;

    Cook() : ID(IDs) {
        this->coord = std::make_pair(0, 0);
        Cook::IDs++;
    }

    Cook(int cid) { this->ID = cid; }

    void next_state() {
        switch (state) {
            case IDLE:
                state = COOKING;
                break;
            case COOKING:
                state = IDLE;
                break;
        }
    }

    void redraw(WINDOW *CooksWindow) {

        int pair = 0;
        switch (state) {
            case IDLE:
                pair = 2;
                break;
            case COOKING:
                pair = 3;
                break;
        }
        wattron(CooksWindow, COLOR_PAIR(pair) | A_BOLD);
        mvwaddch(CooksWindow, this->coord.second, this->coord.first, ACS_DIAMOND);
        wattroff(CooksWindow, COLOR_PAIR(pair) | A_BOLD);
    }
};

int Cook::IDs = 0;
Cook Cook::None(-1);

class Cooks {
private:
    std::vector <Cook> cooks;

public:

    Cooks() {}

    Cooks(int count) { this->cooks = std::vector<Cook>(count); }

    size_t getSize() { return cooks.size(); }

    std::vector <Cook> &getCooks() { return cooks; }

    Cook &getCook(Cook::STATE state) {
        for (Cook &cook : cooks) {
            if (cook.state == state) {
                return cook;
            }
        }
        return Cook::None;
    }

    Cook &getCook(int cid) {
        for (Cook &cook : cooks) {
            if (cook.ID == cid) {
                return cook;
            }
        }
        return Cook::None;
    }

    void redraw(WINDOW *CooksWindow) {
        for (Cook &cook : cooks) {
            cook.redraw(CooksWindow);
            mvwprintw(CooksWindow, cook.coord.second - 1, cook.coord.first - 2, "%3.f%%",
                      cook.progress);
        }
    }
};