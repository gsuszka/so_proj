#pragma once
#include "Buffer.h"
#include "Client.h"
#include "Cook.h"
#include "Table.h"
#include "Waiter.h"

WINDOW *CooksWindow;
WINDOW *BufferWindow;
WINDOW *RoomWindow;
WINDOW *ClientsWindow;
WINDOW *LogWindow;

void initColor() {
    start_color();
    init_pair(1, COLOR_WHITE, COLOR_BLACK);
    init_pair(2, COLOR_GREEN, COLOR_BLACK);
    init_pair(3, COLOR_RED, COLOR_BLACK);
    init_pair(4, COLOR_BLUE, COLOR_BLACK);
    init_pair(5, COLOR_YELLOW, COLOR_BLACK);
}

void drawCooks(WINDOW *wnhdl, Cooks &cooks) {
    int next_x = 3, next_y = 2;
    int cx = next_x, cy = next_y;
    for (Cook &cook : cooks.getCooks()) {
        cook.coord = std::make_pair(cx, cy);
        cook.redraw(wnhdl);
        next_x = cx + 5;
        if (next_x >= wnhdl->_maxx) {
            cx = 3;
            cy += 2;
        } else {
            cx = next_x;
        }
    }
}

void drawTables(WINDOW *wnhdl, Tables &tables) {
    int next_x = 2, next_y = 4;
    int tx = next_x, ty = next_y;
    for (Table &table : tables.getTables()) {
        table.coord = std::make_pair(tx, ty);
        table.redraw(wnhdl);
        next_x = tx + 4;
        if (next_x > wnhdl->_maxx) {
            ty += 5;
            tx = 2;
        } else {
            tx = next_x;
        }
    }
}

void drawClients(WINDOW *ClientsWindow, WINDOW *RoomWindow, Clients &clients) {
    int next_x = 1, next_y = 1;
    int cx = next_x, cy = next_y;
    for (Client &client : clients.getClients()) {
        if (client.state == Client::OUTSIDE) {
            client.coord = std::make_pair(cx, cy);
            client.redraw(ClientsWindow, RoomWindow);
            next_x = cx + 1;
            if (next_x >= ClientsWindow->_maxx) {
                cx = 1;
                cy += 1;
            } else {
                cx = next_x;
            }
        }
    }
}

void drawWaiters(WINDOW *wnhdl, Waiters &waiters) {
    int next_x = 1, next_y = 2;
    int wx = next_x, wy = next_y;
    wattron(wnhdl, COLOR_PAIR(3) | A_BOLD);
    for (Waiter &waiter : waiters.getWaiters()) {
        waiter.bcoord = std::make_pair(wx, wy);
        waiter.redraw(wnhdl);
        next_x = wx + 1;
        if (next_x >= wnhdl->_maxx) {
            wy += 1;
            wx = 1;
        } else {
            wx = next_x;
        }
    }
    wattroff(wnhdl, COLOR_PAIR(5) | A_BOLD);
}

void drawBuffer(WINDOW *BufferWindow, Buffer buffer) {
    int next_x = 1, next_y = 1;
    int dishSize = 3;
    int x = next_x, y = next_y;
    wattron(BufferWindow, COLOR_PAIR(5) | A_BOLD);

    for (auto bc = 0; bc < buffer.buffer; bc++) {
        mvwaddch(BufferWindow, 1, 1 + bc * dishSize, '[');
        waddch(BufferWindow, 'o');
        waddch(BufferWindow, ']');
        next_x = x + dishSize;
        if (next_x >= BufferWindow->_maxx) {
            y += 1;
            x = 1;
        } else {
            x = next_x;
        }
    }
    wattroff(BufferWindow, COLOR_PAIR(5) | A_BOLD);
}
