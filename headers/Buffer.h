#pragma once

#include "figures.h"
#include "../threads/ThreadsGlobals.h"

struct Buffer {
    int max_size;
    int buffer;

    Buffer() {}

    Buffer(int max_size) {
        this->max_size = max_size;
        this->buffer = 0;
    }

    void increment_buffer() {
        if (buffer < max_size) {
            buffer++;
        }
    }

    void decrement_buffer() {
        if (buffer > 0) {
            buffer--;
        }
    }

    bool isFull() { return max_size == buffer ? true : false; }

    bool isEmpty() { return buffer > 0 ? false : true; };

};