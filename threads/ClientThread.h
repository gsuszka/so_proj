#pragma once

#include "../headers/globals.h"
#include "../headers/helpers.h"
#include <sstream>
#include <unistd.h>

void timer(Client *client, int eatingTime);

void ClientThread(Client *client, int eatingTime) {
    Table *table = &Table::None;

    while (!exiting && client != &Client::None) {

        switch (client->state) {
            case Client::OUTSIDE:
                std::this_thread::sleep_for(std::chrono::seconds(2));
                TablesMUTEX.lock();
                table = &tables.getTable(Table::FREE);
                if (table != &Table::None) {
                    table->next_state(); // -> WAIT_FOR_PREP
                    client->coord = table->coord;
                    client->coord.second += 2;
                    client->next_state();
                }
                TablesMUTEX.unlock();

                break;

            case Client::WAITING:
                if (table->state == Table::BUSY) {
                    client->next_state();
                }
                break;

            case Client::EATING:
                timer(client, eatingTime);

                client->next_state();

                TablesMUTEX.lock();
                table->next_state();
                TablesMUTEX.unlock();

                break;

            case Client::EXITING:
                client = &Client::None;
                table = &Table::None;
                break;
        }
    }
}

void timer(Client *client, int eatingTime) {
    int ThreadWait = 100;
    long TimeInMs = 1000 * eatingTime;

    for (long t = 0; t < TimeInMs / ThreadWait; t++) {
        if (exiting) {
            break;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(ThreadWait));
        client->progress = (float) t * 100 / (TimeInMs / ThreadWait);
    }

    client->progress = 100;
}
