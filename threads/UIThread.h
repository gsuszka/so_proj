#pragma once

#include "../headers/globals.h"
#include "../headers/inits.h"

void UIThread(int height) {
	// std::string title("RESTAURACJA");

	drawCooks(CooksWindow, cooks);
	drawTables(RoomWindow, tables);
	drawClients(ClientsWindow, RoomWindow, clients);
	drawWaiters(RoomWindow, waiters);

	box(ClientsWindow, ACS_VLINE, ACS_HLINE);
	box(LogWindow, ACS_VLINE, ACS_HLINE);
	box(RoomWindow, ACS_VLINE, ACS_HLINE);
	box(BufferWindow, ACS_VLINE, ACS_HLINE);
	box(CooksWindow, ACS_VLINE, ACS_HLINE);

	int pressedKey = 0;
	int prevPressedKey = 0;

	Log LOG(1);

	while (prevPressedKey != 'q') {
		pressedKey = getch();

		prevPressedKey = pressedKey != -1 && pressedKey != prevPressedKey ? pressedKey : prevPressedKey;

        wclear(CooksWindow);
        wclear(RoomWindow);
        wclear(BufferWindow);
        wclear(ClientsWindow);
        wclear(LogWindow);

		CooksMUTEX.lock();
		cooks.redraw(CooksWindow);
		CooksMUTEX.unlock();

		TablesMUTEX.lock();
		tables.redraw(RoomWindow);
		TablesMUTEX.unlock();

		BufferMUTEX.lock();
		drawBuffer(BufferWindow, buffer);
		BufferMUTEX.unlock();

		ClientsMUTEX.lock();
		clients.redraw(ClientsWindow, RoomWindow);
		ClientsMUTEX.unlock();

		WaitersMUTEX.lock();
		waiters.redraw(RoomWindow);
		WaitersMUTEX.unlock();

		LogMUTEX.lock();
		switch (prevPressedKey) {
			default:
			case '1':
				LOG.changeSize(cooks.getSize() < height ? cooks.getSize() : height);
				CooksMUTEX.lock();
				for (Cook &item : cooks.getCooks())
					LOG.Add(Log::Serialize(item));
				CooksMUTEX.unlock();
				break;
			case '2':
				BufferMUTEX.lock();
				LOG.changeSize(1);
				LOG.Add(Log::Serialize(buffer));
				BufferMUTEX.unlock();
				break;
			case '3':
				ClientsMUTEX.lock();
				LOG.changeSize(clients.getSize() < height ? clients.getSize() : height);
				for (Client &item : clients.getClients())
					LOG.Add(Log::Serialize(item));
				ClientsMUTEX.unlock();
				break;
			case '4':
				WaitersMUTEX.lock();
				LOG.changeSize(waiters.getSize() < height ? waiters.getSize() : height);
				for (Waiter &item : waiters.getWaiters())
					LOG.Add(Log::Serialize(item));
				WaitersMUTEX.unlock();
				break;
			case '5':
				TablesMUTEX.lock();
				LOG.changeSize(tables.getSize() < height ? tables.getSize() : height);
				for (Table &item : tables.getTables())
					LOG.Add(Log::Serialize(item));
				TablesMUTEX.unlock();
				break;
		}

		wprintw(LogWindow, "\n ");
		for (auto log : LOG.getLogs()) {
			wprintw(LogWindow, "%s\n ", log.c_str());
		}
		LogMUTEX.unlock();

        box(ClientsWindow, ACS_VLINE, ACS_HLINE);
        box(LogWindow, ACS_VLINE, ACS_HLINE);
        box(RoomWindow, ACS_VLINE, ACS_HLINE);
        box(BufferWindow, ACS_VLINE, ACS_HLINE);
        box(CooksWindow, ACS_VLINE, ACS_HLINE);

		wnoutrefresh(CooksWindow);
		wnoutrefresh(BufferWindow);
		wnoutrefresh(LogWindow);
		wnoutrefresh(ClientsWindow);
		wnoutrefresh(RoomWindow);

		doupdate();
		
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
	exiting = true;
}
