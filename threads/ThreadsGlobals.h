
#include <mutex>
#include <thread>

static bool exiting = false;

/// M U T E X E S
static std::mutex CooksMUTEX;
static std::mutex TablesMUTEX;
static std::mutex ClientsMUTEX;
static std::mutex BufferMUTEX;
static std::mutex LogMUTEX;
static std::mutex WaitersMUTEX;
