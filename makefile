main: main.cpp 
	g++ -o main main.cpp -lncurses -lpthread -std=c++14 

clean:
	rm -f main
	make main

run: main.cpp 
	g++ -o main main.cpp -lncurses -lpthread -std=c++14
	./main -t 10 -w 5 -c 5 -s 5 -b 50
	rm -r ./main
